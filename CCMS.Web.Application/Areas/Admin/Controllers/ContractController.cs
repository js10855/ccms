using CCMS.Web.Application.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Net;


namespace CCMS.Web.Application.Areas.Admin.Views.Contract
{
    public class ContractController : Controller
    {
        private const string CONTRACT_VIEWS_PATH = "~/Areas/Admin/Views/Contract/";
        private const string CONTRACT_PARTIAL_VIEWS_PATH = CONTRACT_VIEWS_PATH + "Partials/";
        private const string CONTRACT_NOT_IMPORTED_CONTRACTS_VIEW_PATH = CONTRACT_PARTIAL_VIEWS_PATH + "_NotImportedContracts.cshtml";
        private const string CONTRACT_IMPORT_CONTRACTS_FORM_VIEW_PATH = CONTRACT_PARTIAL_VIEWS_PATH + "_ImportContractsForm.cshtml";

        // GET: Admin/Contract
        public ActionResult Index()
        {
            return View("~/Areas/Admin/Views/Contract/Partials/_ImportContractsForm.cshtml");
        }

        /// <summary>
        /// Action to render import contracts form inside bootstrap popup control
        /// </summary>
        /// <returns>Import contracts form partial view</returns>
        public ActionResult LoadImportContractsForm()
        {
            var viewModel = new ImportContractsVM();
            viewModel.NotImportedContracts = new List<NotImportedContractsVM>();

            return PartialView(CONTRACT_IMPORT_CONTRACTS_FORM_VIEW_PATH, viewModel);
        }

        /// <summary>
        /// Action to retrieve data from PreConstruction using static json/Web API call as source to map required data elements
        /// </summary>
        /// <param name="viewModel">ImportContractsVM page viewmodel</param>
        /// <returns>Filtered contracts results based on the input criteria passed by ImportContractsVM view model</returns>
        [HttpPost]
        public ActionResult GetContracts(ImportContractsVM viewModel)
        {
            viewModel.NotImportedContracts = new List<NotImportedContractsVM>();

            bool isContractNumber = false;
            if (string.IsNullOrEmpty(viewModel.ContractNumber))
            {
                if (!viewModel.StartDate.HasValue)
                {
                    ModelState.AddModelError("StartDate", "From Date is required.");
                }
                if (!viewModel.EndDate.HasValue)
                {
                    ModelState.AddModelError("EndDate", "To Date is required.");
                }
            }
            else
            {
                isContractNumber = true;
                if (!viewModel.StartDate.HasValue || !viewModel.EndDate.HasValue)
                {
                    ModelState.AddModelError("ContractNumber", "Please select either date range or Contract Number.");
                }

            }
            if (true)//(ModelState.IsValid)           
            {
                try
                {
                    List<ProposalVM> proposalInfo = null;

                    //WriteToLog("File Path:  " + Server.MapPath(ConfigurationManager.AppSettings["JsonFilePath"]));

                    //Test and Delete-local-Read Proposal info from json file 
                    var readJsonData = "";
                    using (StreamReader r = new StreamReader(Server.MapPath("~/Content/ApiData/PreConContractDetails.json")))
                    {
                        readJsonData = r.ReadToEnd();
                    }
                    var serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = Int32.MaxValue;

                    var json2Model = serializer.Deserialize<List<NotImportedContractsVM>>(readJsonData);
                    var result = serializer.Deserialize<List<ProposalVM>>(readJsonData);
                    //if (result.Count!= 0)
                    //{
                    //    proposalInfo = (new JavaScriptSerializer()).Deserialize<List<ProposalVM>>(result.ToString());//.Deserialize<List<ProposalVM>>(result.Result)
                    //}
                    //End of Reading from JSon file

                    //Web API code -Original
                    ////string contractWebApiUrl = ConfigurationManager.AppSettings["PreConstructionListApi"];

                    ////if (isContractNumber)
                    ////{
                    ////    contractWebApiUrl = string.Format(contractWebApiUrl, viewModel.ContractNumber, string.Empty, string.Empty);
                    ////}
                    ////else
                    ////{
                    ////    contractWebApiUrl = string.Format(contractWebApiUrl, string.Empty, viewModel.StartDate.Value.Date, viewModel.EndDate.Value.Date);
                    ////}

                    ////WriteToLog("GetWebApiUrl " + contractWebApiUrl);
                    ////proposalInfo = GetContractInfoFromPreConstruction(proposalInfo, contractWebApiUrl);
                    /////End of original web API code

                    //Note: Below line is only needed by Jigar
                    CCMSDBEntities db = new CCMSDBEntities();

                    foreach (var data in result)// foreach (var data in proposalInfo)
                    {
                        NotImportedContractsVM notImportedContracts = new NotImportedContractsVM();
                        notImportedContracts.ContractId = data.PROPOSALID;
                        notImportedContracts.ContractNumber = data.PROPOSALNO;
                        notImportedContracts.Description = data.LONGDESCRIPTION;
                        notImportedContracts.Status = data.PROPOSALSTATUS;
                        notImportedContracts.AwardedDate = data.AWARDEDDATE;

                        //Note: below line is changed by Jigar as per his code but no need to change it.
                        //var contract = this._context.Contracts.Get(c => c.ContNo == data.PROPOSALNO).FirstOrDefault();
                        var contract = db.contracts.Where(c => c.C_cont_no_ == data.PROPOSALNO).FirstOrDefault();
                        if (contract != null)
                            notImportedContracts.IsExistingRecord = true;
                        viewModel.NotImportedContracts.Add(notImportedContracts);
                    }

                }
                catch (Exception ex)
                {
                    WriteToLog("Error#: " + ex.InnerException + " Message: " + ex.Message);
                    //Note: below line is commented by Jigar as Not needed for him
                    //LogUtility.LogError("Error importing contracts", ex);
                    ModelState.AddModelError("", ex.InnerException.Message ?? ex.Message);
                    ViewData["EditErrors"] = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).ToArray();
                }
            }
            //else
            //{
            //    ViewData["EditErrors"] = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).ToArray();
            //}

            //viewModel.ErrorMessage = String.Join("<br />", ((string[])ViewData["EditErrors"]));

            return PartialView(CONTRACT_NOT_IMPORTED_CONTRACTS_VIEW_PATH, viewModel.NotImportedContracts);
        }

        /// <summary>
        /// Action to perform contract related information insert/update operation into e-MCMS PostgreSQL database using json/Web API call as source to map required data elements
        /// </summary>
        /// <param name="viewModel">NotImportedContractsVM page viewmodel</param>
        /// <returns>Currently Redirects/Refreshes to Admin Contracts datagrid</returns>
        [HttpPost]
        public ActionResult InsertContracts(string contracts)
        {
            //TODO: Complete logic
            var myarray = contracts.Split(',');

            bool contractCompleteImportFailed = false;
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("<ul>");

            List<ProposalVM> proposalInfo = null;

            string contractWebApiUrl = ConfigurationManager.AppSettings["PreConstructionDetailsApi"];

            for (var i = 0; i < myarray.Length; i++)
            {
                contractCompleteImportFailed = false;
                int contractId = 0;
                int? estimateSetId = 0;
                int? projectSetId = 0;
                int? firmId = null;
                int? itemSetId = 0;
                int bidItemId = 0;

                contractWebApiUrl = string.Format(contractWebApiUrl, string.Empty, myarray[i], string.Empty, string.Empty);
                WriteToLog("InsertWebApiUrl: " + contractWebApiUrl);                
                var result = GetContractInfoFromPreConstruction(proposalInfo, contractWebApiUrl).FirstOrDefault();

                DateTime dt = DateTime.Now;

                //Note: Below line is only needed by Jigar
                CCMSDBEntities db = new CCMSDBEntities();

                #region Contract                
                try
                {
                    var contract = db.contracts.Where(r => r.C_cont_no_ == result.PROPOSALNO).FirstOrDefault();

                    if (contract == null)
                    {
                        //contract = new contract();
                        //contract.OrgId = 3;//TODO
                        //contract.DeptId = 43;
                        //contract.CStateId = "N";
                        //contract.ContNo = result.PROPOSALNO;
                        //contract.Cont2No = null;
                        //contract.FapNo = result.FAPNO1;
                        //contract.FapNo2 = result.FAPNO2;
                        //contract.FapNo3 = result.FAPNO3;
                        //contract.Title = result.SHORTDESCRIPTION;
                        //contract.Descr = result.LONGDESCRIPTION;
                        //contract.AllowMultipleProjects = false;
                        //contract.AllowMultipleEstimateSets = false;
                        //contract.AllowMultipleItemSets = false;
                        //contract.AllowMultipleProjectSets = false;
                        //contract.OrigValue = result.AWARDAMOUNT;
                        //contract.OrigDays = result.ORIGDAYS;
                        //contract.CostMethodId = 1;
                        //contract.EndDate = result.ENDDATE;
                        //contract.CreateUserId = "Appsetting";
                        //contract.CreateTimestamp = dt;
                        //contract.ModUserId = "Appsetting";
                        //contract.ModTimestamp = dt;
                        //this._context.Contracts.Insert(contract);
                        //this._context.Commit(AppHelper.CurrentUser.UserID);

                        //contractId = contract.Id;
                    }
                }
                catch (Exception ex)
                {
                    //strBuilder.Append("<li>Contract Info</li>");
                    contractCompleteImportFailed = true;
                    WriteToLog("Error# Contract: " + ex.InnerException + " Message: " + ex.Message);
                }
                #endregion

                #region Contract Details
                //try
                //{
                //    var contractDetails = this._context.ContractDetails.Get(r => r.CId == contractId).FirstOrDefault();
                //    if (contractDetails != null)
                //    {
                //        //contractDetails = new ContractDetail();
                //        //contractDetails.CId = contractId;
                //        contractDetails.AdvertiseDate = result.ADVERTISEMENTDATE; //Advertizement date;
                //        contractDetails.AwardDate = result.AWARDEDDATE;//awardeddate;
                //        contractDetails.BidDate = result.BIDDATE;

                //        //contractDetails.ModUserId = model.Details.MinAdjustmentAllowed;
                //        //contractDetails.ModTimestamp = model.Details.MaxAdjustmentAllowed;

                //        this._context.ContractDetails.Update(contractDetails);
                //        this._context.Commit(AppHelper.CurrentUser.UserID);

                //    }
                //}
                //catch (Exception ex)
                //{
                //    //strBuilder.Append("<li>Contract Details Info</li>");
                //    contractCompleteImportFailed = true;
                //    WriteToLog("Error# ContractDetails: " + ex.InnerException + " Message: " + ex.Message);
                //}
                #endregion

                #region Contract Status
                //try
                //{
                //var contractStatus = this._context.ContractStatus.Get(c => c.CId == contractId).FirstOrDefault();
                //if (contractStatus == null)
                //{
                //    contractStatus = new ContractStatus();
                //    contractStatus.CId = contractId;
                //    contractStatus.CfgLevels = 1;
                //    contractStatus.NumEstSets = 1;
                //    contractStatus.NumItemSets = 1;
                //    contractStatus.NumProjects = 1;
                //    contractStatus.NumProjectsOpen = 1;
                //    contractStatus.OrigValue = result.AWARDAMOUNT;//proposal.awardedamount;
                //    contractStatus.OrigValuePrj = result.AWARDAMOUNT;//proposal.awardedamount;
                //    contractStatus.TimeMethodId = result.TIMETYPEID;//proposaltime.timetype;
                //    contractStatus.TimeMethod = result.TIMETYPE;
                //    contractStatus.OrigDaysCnt = result.ORIGDAYS;//orig days
                //    contractStatus.EndDateOrig = result.ENDDATE;//enddate
                //    this._context.ContractStatus.Insert(contractStatus);
                //    this._context.Commit(AppHelper.CurrentUser.UserID);
                //}
                //}
                //catch (Exception ex)
                //{
                //    //strBuilder.Append("<li>Contract Status Info</li>");
                //    contractCompleteImportFailed = true;
                //}
                #endregion

                //#region Firm                
                //try
                //{
                //    //var firmList = result.FirmList;
                //    if (result.FirmList != null && result.FirmList.Count() > 0)
                //    {
                //        foreach (var item in result.FirmList)
                //        {
                //            var firm = this._context.Firms.Get(r => r.Fein == item.FEDIDNO && r.OrgId == 3).FirstOrDefault();
                //            if (firm == null)
                //            {
                //                firm = new Firm();
                //                firm.OrgId = 3;
                //                firm.FirmName = item.VENDORNAME;
                //                firm.Addr1 = item.ADDRESS1;
                //                firm.Addr2 = item.ADDRESS2;
                //                firm.City = item.CITY;
                //                firm.State = item.STATE;
                //                firm.Zip = item.ZIPCODE;
                //                firm.Fein = item.FEDIDNO;
                //                firm.Mbe = item.CERTIFICATIONTYPE;
                //                //firm.CreateUserId = "";
                //                //firm.CreateTimestamp = "";
                //                //firm.ModUserId = "";
                //                //firm.ModTimestamp = "";
                //                this._context.Firms.Insert(firm);
                //                this._context.Commit(AppHelper.CurrentUser.UserID);

                //                firmId = firm.Id;
                //            }
                //            else
                //            {
                //                firmId = firm.Id;
                //            }
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{
                //    //strBuilder.Append("<li>Firm Info</li>");
                //    contractCompleteImportFailed = true;
                //    WriteToLog("Error# Firms: " + ex.InnerException + " Message: " + ex.Message);
                //}
                //#endregion

                //#region Estimate Sets                
                //try
                //{
                //    var estimateSets = this._context.EstimateSets.Get(r => r.CId == contractId).FirstOrDefault();
                //    if (estimateSets == null)
                //    {
                //        estimateSets = new EstimateSet();
                //        estimateSets.CId = contractId;
                //        estimateSets.EstSet = result.PROPOSALNO; //proposal. no
                //        estimateSets.Description = result.PROPOSALNO + " Estimates Sets";// //proposal. no + 
                //        estimateSets.LockoutMethod = "Single";
                //        estimateSets.PrimeFirmId = firmId;
                //        //estimateSets.CreateUserId = "";
                //        //estimateSets.CreateTimestamp = "";
                //        //estimateSets.ModUserId = "";
                //        //estimateSets.ModTimestamp = "";

                //        this._context.EstimateSets.Insert(estimateSets);
                //        this._context.Commit(AppHelper.CurrentUser.UserID);

                //        estimateSetId = estimateSets.Id;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    //strBuilder.Append("<li>Estimate Sets Info</li>");
                //    contractCompleteImportFailed = true;
                //}
                //#endregion

                //#region Project Sets                
                /////TODO: multiple possible
                //try
                //{
                //    var projectSets = this._context.ProjectSets.Get(r => r.CId == contractId).FirstOrDefault();
                //    if (projectSets == null)
                //    {
                //        projectSets = new ProjectSet();

                //        projectSets.CId = contractId;
                //        projectSets.PSet = result.PROPOSALNO;
                //        projectSets.WorkflowId = 1;
                //        //projectSets.CreateUserId = "";
                //        //projectSets.CreateTimestamp = "";
                //        //projectSets.ModUserId = "";
                //        //projectSets.ModTimestamp = "";
                //        this._context.ProjectSets.Insert(projectSets);
                //        this._context.Commit(AppHelper.CurrentUser.UserID);

                //        projectSetId = projectSets.Id;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    //strBuilder.Append("<li>Project Sets Info</li>");
                //    contractCompleteImportFailed = true;
                //    WriteToLog("Error# ProjectSet: " + ex.InnerException + " Message: " + ex.Message);
                //}
                //#endregion

                //#region Item Sets                 
                //try
                //{
                //    ItemSet itemSets = new ItemSet();
                //    itemSets.EstSetId = estimateSetId ?? 0;
                //    itemSets.ItemSetStateId = "N";
                //    //itemSets.ItemSet = result.PROPOSALNO;
                //    itemSets.IncludeRetainage = true;
                //    itemSets.RetainageCap = .05;
                //    itemSets.RetainagePerPay = .05;
                //    //projectSets.ModTimestamp = "";
                //    this._context.ItemSets.Insert(itemSets);
                //    this._context.Commit(AppHelper.CurrentUser.UserID);

                //    itemSetId = itemSets.Id;
                //}
                //catch (Exception ex)
                //{
                //    //strBuilder.Append("<li>Item Sets Info</li>");
                //    contractCompleteImportFailed = true;
                //    WriteToLog("Error# ItemSet: " + ex.InnerException + " Message: " + ex.Message);
                //}
                //#endregion

                //#region Project Contractor                 
                //try
                //{
                //    ProjectContractor projectContractor = new ProjectContractor();
                //    projectContractor.PSetId = projectSetId ?? 0;
                //    projectContractor.FirmId = firmId ?? 0;
                //    // projectContractor.FirmNameAbbreviation = result.
                //    projectContractor.IsPrime = true;
                //    //projectSets.CreateUserId = "";
                //    //projectSets.CreateTimestamp = "";
                //    //projectSets.ModUserId = "";
                //    //projectSets.ModTimestamp = "";
                //    this._context.ProjectContractors.Insert(projectContractor);
                //    this._context.Commit(AppHelper.CurrentUser.UserID);
                //}
                //catch (Exception ex)
                //{
                //    //strBuilder.Append("<li>Project Contractor Info</li>");
                //    contractCompleteImportFailed = true;
                //    WriteToLog("Error# ProjectContractor: " + ex.InnerException + " Message: " + ex.Message);
                //}
                //#endregion

                //#region Contract Fund Package
                //if (result.ContractFundPackageList != null && result.ContractFundPackageList.Count() > 0)
                //{
                //    foreach (var contFund in result.ContractFundPackageList)
                //    {
                //        try
                //        {
                //            ContractFund contractFund = new ContractFund();
                //            contractFund.CId = contractId;
                //            contractFund.FundCode = contFund.FUNDPACKAGECODE;
                //            contractFund.Descr = contFund.FUNDPACKAGENM;
                //            contractFund.FundPackageId = !string.IsNullOrEmpty(contFund.FUNDPACKAGEID) ? Convert.ToInt32(contFund.FUNDPACKAGEID) : (int?)null;
                //            contractFund.Is3rdParty = false;
                //            //projectSets.CreateUserId = "";
                //            //projectSets.CreateTimestamp = "";
                //            //projectSets.ModUserId = "";
                //            //projectSets.ModTimestamp = "";
                //            this._context.ContractFunds.Insert(contractFund);
                //            this._context.Commit(AppHelper.CurrentUser.UserID);
                //        }
                //        catch (Exception ex)
                //        {
                //            //strBuilder.Append("<li>Contract Funds Info</li>");
                //            contractCompleteImportFailed = true;
                //            WriteToLog("Error# Contract Funds Info: " + ex.InnerException + " Message: " + ex.Message);
                //        }
                //    }
                //}
                //#endregion

                //#region Projects
                //if (result.ProjectList != null && result.ProjectList.Count() > 0)
                //{
                //    foreach (var proj in result.ProjectList)
                //    {
                //        int projectId = 0;

                //        try
                //        {
                //            Project project = new Project();
                //            project.CId = contractId;
                //            project.PSetId = projectSetId ?? 0;
                //            project.ItemSetId = itemSetId ?? 0;
                //            project.EstSetId = estimateSetId ?? 0;
                //            project.PStateId = "N";
                //            project.PNo = result.PROPOSALNO;
                //            project.Title = proj.PROJECTTITLE;
                //            project.Descr = proj.PROJECTDESCRIPTION;
                //            project.TimeZone = "EST";
                //            project.StateCode = "MD";
                //            project.TimeMethodId = result.TIMETYPEID;
                //            project.OrigValue = result.AWARDAMOUNT;
                //            project.OrigDays = result.ORIGDAYS;
                //            project.EndDate = result.ENDDATE;

                //            //projectSets.CreateUserId = "";
                //            //projectSets.CreateTimestamp = "";
                //            //projectSets.ModUserId = "";
                //            //projectSets.ModTimestamp = "";

                //            this._context.Projects.Insert(project);
                //            this._context.Commit(AppHelper.CurrentUser.UserID);

                //            projectId = project.Id;
                //        }
                //        catch (Exception ex)
                //        {
                //            //strBuilder.Append("<li>Project Contractor Info</li>");
                //            contractCompleteImportFailed = true;
                //            WriteToLog("Error# Project: " + ex.InnerException + " Message: " + ex.Message);
                //        }

                //        //ProjectDetail projectDetail = new ProjectDetail();
                //        //projectDetail.PId = projectId;
                //        //projectDetail.BidDate = result.BIDDATE;
                //        //projectDetail.AdvertiseDate = result.ADVERTISEMENTDATE;
                //        //projectDetail.AwardDate = result.AWARDEDDATE;
                //        ////projectDetail.ModUserId = "";
                //        //this._context.ProjectDetails.Insert(projectDetail);
                //        //this._context.Commit(AppHelper.CurrentUser.UserID);


                //        if (proj.ProjectItemsList != null && proj.ProjectItemsList.Count() > 0)
                //        {
                //            foreach (var projItem in proj.ProjectItemsList)
                //            {
                //                if (!string.IsNullOrEmpty(projItem.PROPOSALITEMLINENUMBER))
                //                {
                //                    try
                //                    {
                //                        ItemBase itemBase = new ItemBase();
                //                        itemBase.ItemSetId = itemSetId ?? 0;
                //                        itemBase.ItemIndex = !string.IsNullOrEmpty(projItem.PROPOSALITEMLINENUMBER) ? Convert.ToInt32(projItem.PROPOSALITEMLINENUMBER) : 0;

                //                        int categoryId = 0;
                //                        if (!string.IsNullOrEmpty(projItem.PROPOSALITEMLINENUMBER) && projItem.PROPOSALITEMLINENUMBER.Length > 0)
                //                        {
                //                            categoryId = Convert.ToInt32(projItem.PROPOSALITEMLINENUMBER.Substring(0, 1));
                //                        }

                //                        itemBase.CatId = categoryId;
                //                        itemBase.Descr = projItem.PROJECTITEMDESCRIPTION;
                //                        itemBase.Unit = projItem.UOM;
                //                        itemBase.UnitPrice = projItem.ENG_PRICE ?? 0;//TODO
                //                        itemBase.CatCode = projItem.REFITEMNM;
                //                        itemBase.SpecNo = projItem.SPECYR;
                //                        //projectSets.ModTimestamp = "";
                //                        this._context.ItemBases.Insert(itemBase);
                //                        this._context.Commit(AppHelper.CurrentUser.UserID);

                //                        bidItemId = itemBase.Id;
                //                    }
                //                    catch (Exception ex)
                //                    {
                //                        //strBuilder.Append("<li>Item Base Info</li>");
                //                        contractCompleteImportFailed = true;
                //                        WriteToLog("Error# ItemBase: " + ex.InnerException + " Message: " + ex.Message);
                //                    }

                //                    try
                //                    {
                //                        ItemDatum itemData = new ItemDatum();
                //                        itemData.BitemId = bidItemId;
                //                        itemData.Init = true;
                //                        itemData.ItemName = projItem.PROPOSALITEMLINENUMBER + ".01 ";//TODO
                //                        itemData.FundId = null;//TODO
                //                        itemData.Section = "01";
                //                        itemData.OrigQty = projItem.PROJQTY != null ? Convert.ToDouble(projItem.PROJQTY) : (double)0;
                //                        itemData.InitQty = projItem.PROJQTY != null ? Convert.ToDouble(projItem.PROJQTY) : (double)0;
                //                        itemData.AuthQty = projItem.PROJQTY != null ? Convert.ToDouble(projItem.PROJQTY) : (double)0;
                //                        itemData.ContractorId = null;//TODO
                //                                                     //projectSets.ModTimestamp = "";
                //                        this._context.ItemData.Insert(itemData);
                //                        this._context.Commit(AppHelper.CurrentUser.UserID);
                //                    }
                //                    catch (Exception ex)
                //                    {
                //                        //strBuilder.Append("<li>Item Data Info</li>");
                //                        contractCompleteImportFailed = true;
                //                        WriteToLog("Error# ItemDatum: " + ex.InnerException + " Message: " + ex.Message);
                //                    }
                //                }

                //            }
                //        }

                //        //if (result.ContractFundsList != null && result.ContractFundsList.Count() > 0)
                //        //{
                //        //    foreach (var contFund in result.ContractFundsList)
                //        //    {
                //        //        try
                //        //        {
                //        //            ContractFund contractFund = new ContractFund();
                //        //            contractFund.CId = contractId;
                //        //            contractFund.FundCode = contFund.FUNDID;
                //        //            contractFund.Is3rdParty = false;
                //        //            //projectSets.CreateUserId = "";
                //        //            //projectSets.CreateTimestamp = "";
                //        //            //projectSets.ModUserId = "";
                //        //            //projectSets.ModTimestamp = "";
                //        //            this._context.ContractFunds.Insert(contractFund);
                //        //            this._context.Commit(AppHelper.CurrentUser.UserID);
                //        //        }
                //        //        catch (Exception ex)
                //        //        {
                //        //            //strBuilder.Append("<li>Contract Funds Info</li>");
                //        //            contractCompleteImportFailed = true;
                //        //        }
                //        //    }
                //        //}


                //    }
                //}
                //#endregion

                string importedStatus = "Imported Successfully";
                if (contractCompleteImportFailed)
                    importedStatus = "Failed to import successfully";
                strBuilder.Append(string.Format("<li><b>Contract:</b> {0} <b>Status:</b> {1}</li>", result.PROPOSALNO, importedStatus));
            }

            strBuilder.Append("</ul>");
            //return View(CONTRACT_VIEWS_PATH + "Index.cshtml");            
            return Json(new { success = true, ImportedStatus = strBuilder.ToString() }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Web API call to retrieve Proposal info from Pre-Construction
        /// </summary>
        /// <param name="proposalInfo">Object view model</param>
        /// <param name="contractWebApiUrl">Web API url</param>
        /// <returns>Deserialized json object</returns>
        private static List<ProposalVM> GetContractInfoFromPreConstruction(List<ProposalVM> proposalInfo, string contractWebApiUrl)
        {
            //HttpClient httpClient = new HttpClient();
            //HttpClientHandler httpHandler = null;
            //HttpResponseMessage response = null;

            //try
            //{

            //    httpHandler = new HttpClientHandler { UseDefaultCredentials = true };
            //    httpClient = new HttpClient(httpHandler) { BaseAddress = new Uri(contractWebApiUrl) };
            //    response = httpClient.GetAsync(contractWebApiUrl).Result;
            //    var result = response.Content.ReadAsStringAsync();
            //    response.EnsureSuccessStatusCode();

            //    if (result.Result.Length > 2 && response.IsSuccessStatusCode && response.StatusCode == System.Net.HttpStatusCode.OK)
            //    {
            //        proposalInfo = (new JavaScriptSerializer()).Deserialize<List<ProposalVM>>(result.Result);
            //    }

            //}
            //catch (ConfigurationErrorsException configException)
            //{
            //    throw configException;
            //}
            //catch (ArgumentNullException nullException)
            //{
            //    throw nullException;
            //}
            //catch (HttpRequestException httpRequestException)
            //{
            //    throw httpRequestException;
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //finally
            //{
            //    httpClient.Dispose();
            //}

            return proposalInfo;
        }

        /// <summary>
        /// To record traces
        /// </summary>
        /// <param name="strTraceInfo">Trace string to write</param>
        /// <returns>Success or Error string</returns>
        public String WriteToLog(string strTraceInfo)
        {
            string strRet = "Success";
            //string tempFullName = "";
            //tempFullName = User.Identity.Name.ToLower();
            String AppPathStr = Request.PhysicalApplicationPath;
            String FILE_NAME = AppPathStr + @"\TempFiles\" + "App_log.txt";

            StreamWriter objWriter = new StreamWriter(FILE_NAME, true);
            StringBuilder sb = new StringBuilder();

            sb.Append(System.Environment.NewLine);
            sb.Append("--------------Writting New Trace Info-------------------------");
            sb.Append(System.Environment.NewLine);
            sb.Append(strTraceInfo);
            sb.Append(System.Environment.NewLine);
            sb.Append(DateTime.Now);
            sb.Append(System.Environment.NewLine);
            //sb.Append("User: " + tempFullName);
            sb.Append(System.Environment.NewLine);
            try
            {
                objWriter.WriteLine(sb);
                objWriter.Close();
            }
            catch (Exception e)
            {
                strRet = "ERROR in WriteToLog";
                string action = "Index";
                //Response.Redirect(String.Format("~/Error/{0}/?message={1}", action, "ERROR in WriteToLog - " + strTraceInfo));
            }

            return strRet;
        }


       
    }
}