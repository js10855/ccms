﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CCMS.Web.Application.Areas.Admin.Models
{
    public class ImportContractsVM
    {
        [Display(Name = "Contracts Awarded From")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Required]
        public DateTime? StartDate { get; set; }
        [Display(Name = "To")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Required]
        public DateTime? EndDate { get; set; }
        [Required]
        [Display(Name = "Contract Number")]
        public string ContractNumber { get; set; }

        public string ErrorMessage { get; set; }

        public IList<NotImportedContractsVM> NotImportedContracts { get; set; }
    }
}