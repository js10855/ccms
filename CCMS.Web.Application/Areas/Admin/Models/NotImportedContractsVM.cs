﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CCMS.Web.Application.Areas.Admin.Models
{
    public class NotImportedContractsVM
    {


        public string SelectedContracts { get; set; }

        [Display(Name = "Contract Id")]
        public string ContractId { get; set; }

        [Display(Name = "Contract Number")]
        public string ContractNumber { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Awarded Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? AwardedDate { get; set; }

        [Display(Name = "Status")]
        public bool IsExistingRecord { get; set; }

    }
      
    public class ProposalVM
    {
        public string PROPOSALID { get; set; }
        public string PROPOSALNO { get; set; }
        public string SHORTDESCRIPTION { get; set; }
        [MaxLength]
        public string LONGDESCRIPTION { get; set; }
        public string PROPOSALSTATUS { get; set; }
        public Nullable<System.DateTime> ADVERTISEMENTDATE { get; set; }
        public Nullable<System.DateTime> BIDDATE { get; set; }
        public Nullable<System.DateTime> AWARDEDDATE { get; set; }
        public decimal AWARDAMOUNT { get; set; }
        public string AWARDVENDORID { get; set; }
        public string AWDVENDORNO { get; set; }
        public string FAPNO1 { get; set; }
        public string FAPNO2 { get; set; }
        public string FAPNO3 { get; set; }
        public Nullable<System.DateTime> ENDDATE { get; set; }
        public Nullable<System.Int32> ORIGDAYS { get; set; }
        public string TIMETYPEID { get; set; }
        public string TIMETYPE { get; set; }


        //CONTRACT_DETAILS
        //     public Nullable<System.DateTime> PUBLICATION_DT { get; set; }
        //     public Nullable<System.DateTime> LETTING_DT { get; set; }
        //     public Nullable<System.DateTime> STATUS_DT { get; set; }
        // CONTRACT_STATUS
        //     public string MILESTONETYPE { get; set; }

        public IList<Projects> ProjectList { get; set; }

        public IList<Firms> FirmList { get; set; }
        //     public IList<ProjectItem> projectItemsList { get; set; }
        //   Proposal Item
        //    public IList<ProposalItem> proposalItemsList { get; set; }

        public IList<ContractFundPackage> ContractFundPackageList { get; set; }

        public IList<ContractFunds> ContractFundsList { get; set; }

    }
    public class ProposalDtls
    {
        public Nullable<System.DateTime> PUBLICATION_DT { get; set; }
        public Nullable<System.DateTime> LETTING_DT { get; set; }
        public Nullable<System.DateTime> STATUS_DT { get; set; }
    }

    public class Projects
    {
        public string PROJECTID { get; set; }
        public string PROJECTNM { get; set; }
        public string PROJECTTITLE { get; set; }
        public string PROJECTDESCRIPTION { get; set; }
        public string SPECYEAR { get; set; }
        public string FAP_NO1 { get; set; }
        public string FAP_NO2 { get; set; }
        public string FAP_NO3 { get; set; }
        public string PROPOSALID { get; set; }
        public Nullable<decimal> ENGCONSPCT { get; set; }
        public string PROJECTSTATUS { get; set; }
        public string PRIMARYDISTRICTNM { get; set; }
        public string PROJTYPE { get; set; }
        public string PRIMARYCOUNTYID { get; set; }
        public string PRIMARYCOUNTYNM { get; set; }
        //  public string DESIGERID { get; set; }
        // public string DESIGNERNAME { get; set; }
        public Nullable<decimal> LIQUID_DAMAGE_RATE { get; set; }

        public IList<ProjectItem> ProjectItemsList { get; set; }

       

    }

    public class ProjectItem
    {
        public string PROJECTITEMID { get; set; }
        public string PROJECTID { get; set; }
        public string PROJECTNM { get; set; }
        public string PROJECTITEMLINENUMBER { get; set; }
        public string REFITEMID { get; set; }
        public string REFITEMNM { get; set; }
        public string UOM { get; set; }
        public string SPECYR { get; set; }
        public bool NONBIDITEM { get; set; }
        public Nullable<bool> COMBINELIKEITEM { get; set; }
        public Nullable<decimal> PROJQTY { get; set; }
        public Nullable<decimal> ENG_PRICE { get; set; }
        public string PROJECTITEMDESCRIPTION { get; set; }
        public string SUPPLEMENTALDESCRIPTION { get; set; }
        public string CATEGORYID { get; set; }
        public string CATEGORYNM { get; set; }
        public string FUNDPACKAGEID { get; set; }
        public string FUNDPACKAGE_NM { get; set; }
        public string PROPOSALSECTIONID { get; set; }
        public string PROPOSALSECTION_NM { get; set; }
        public string PROPOSALITEMID { get; set; }
        public string PROPOSALITEMLINENUMBER { get; set; }
        public string BIDREQUIREMENTCODE { get; set; }
        public Nullable<decimal> UNITPRICECOMPARISON { get; set; }
    }

    public class ProposalItem
    {
        public string PROPOSALITEMID { get; set; }
        public string PROPOSALID { get; set; }
        public string PROPOSALNM { get; set; }
        public string PROPOSALITEMLINENUMBER { get; set; }
        public string PROPOSALSECTIONID { get; set; }
        public string PROPOSALSECTIONNM { get; set; }
        public string REFITEMID { get; set; }
        public string REFITEMNM { get; set; }
        public string SPECYR { get; set; }
        public decimal PROPOSALQTY { get; set; }
        public string PROPOSALITEMFULLDESCRIPTION { get; set; }
        public string PROPOSALITEMSUPPDESCRIPTION { get; set; }
        public string BIDREQUIREMENTCODE { get; set; }
        public decimal UNITCOMAPARISONPRICE { get; set; }

        public decimal LOWCOSTFLAG { get; set; }

    }

    public class Firms
    {
        public string REFVENDORID { get; set; }
        public string REFVENDORNO { get; set; }
        public string VENDORNAME { get; set; }
        public string VENDORSHORTNAME { get; set; }
        public string FEDIDNO { get; set; }
        public bool? CERTIFICATIONTYPE { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIPCODE { get; set; }
        public string PHONENUMBER { get; set; }
    }

    public class ContractFundPackage
    {
        public string PROPOSALID { get; set; }
        public string CAEGORYID { get; set; }
        public string CATEGORYNM { get; set; }
        public string FUNDPACKAGECODE { get; set; }
        public string FUNDPACKAGEID { get; set; }
        public string FUNDPACKAGENM { get; set; }


    }


    public class ContractFunds
    {
        public string PROJECTID { get; set; }
        public string PROJECTNM { get; set; }
        public string FUNDPACKAGEID { get; set; }
        public string FUNDPAKCAGEDESCR { get; set; }
        public string FUNDID { get; set; }
        public string REFFUNDID { get; set; }
        public string FUNDDESCRIPTION { get; set; }
        public string FUNDTYPE { get; set; }
        public Int32 PERCENTAGE { get; set; }
        public string ACCOUNTINGFUND { get; set; }

    }



}
