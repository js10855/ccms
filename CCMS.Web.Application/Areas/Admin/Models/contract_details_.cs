//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CCMS.Web.Application.Areas.Admin.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class contract_details_
    {
        public string id { get; set; }
        public string c_id { get; set; }
        public string rpt_header { get; set; }
        public string advertise_date { get; set; }
        public string bid_date { get; set; }
        public string award_date { get; set; }
        public string ins_start_date { get; set; }
        public string ins_end_date { get; set; }
        public string min_adjustment_allowed { get; set; }
        public string max_adjustment_allowed { get; set; }
        public string escrow_bank { get; set; }
        public string escrow_addr1 { get; set; }
        public string escrow_addr2 { get; set; }
        public string escrow_city { get; set; }
        public string escrow_state { get; set; }
        public string escrow_zip { get; set; }
        public string escrow_acctno { get; set; }
        public string mod_user_id { get; set; }
        public string mod_timestamp { get; set; }
    
        public virtual contract contract { get; set; }
    }
}
